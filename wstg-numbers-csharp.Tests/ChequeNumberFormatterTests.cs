using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace wstg_numbers_csharp.Tests
{
    [TestClass]
    public class ChequeNumberFormatterTests
    {
        [TestMethod]
        [DataRow(1357256.32, "one million, three hundred and fifty seven thousand, two hundred and fifty six DOLLARS AND thirty two CENTS")]
        [DataRow(57256.32, "fifty seven thousand, two hundred and fifty six DOLLARS AND thirty two CENTS")]
        [DataRow(1357256, "one million, three hundred and fifty seven thousand, two hundred and fifty six DOLLARS")]
        public void DoTest(double currency, string text)
        {
            var formatter = new ChequeNumberFormatter();
            Assert.AreEqual(text, formatter.GetFormatedString((decimal)currency), true);
        }

        [DataRow(1, "one dollar")]
        [DataRow(1.01, "one dollar and one cent")]
        [DataRow(1.1, "one dollar and ten cents")]
        [DataRow(0, "zero dollars")]
        [DataRow(0.01, "zero dollars and one cent")]
        [DataRow(0.1, "zero dollars and ten cents")]
        [TestMethod]
        public void CustomValues(double currency, string text)
        {
            var formatter = new ChequeNumberFormatter();
            Assert.AreEqual(text, formatter.GetFormatedString((decimal)currency), true);
        }
    }
}
