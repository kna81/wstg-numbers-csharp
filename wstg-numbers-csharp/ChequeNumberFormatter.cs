﻿using System;
using System.Diagnostics;
using System.Text;

namespace wstg_numbers_csharp
{
/*
The algorithm builds a string separately for dollars and cents. 
The sum of dollars is separated by groups (by three digits inside) from low digits to high. 
Each group of that are converted in words independently. 
All group representations and cents representation are joined after that.

I guess for improving the speed of the algorithm 
there should use the static calculation of the group. 
Inside the initialization of the algorithm values for any groups 
should be calculated and stored in internal memory. After that, 
the algorithm will be got the values from the cache.

If the memory is not enough 
It's possible to build the cache for the first hundred only.
*/
    /// <summary>
    /// Class for writing money count as string of words
    /// </summary>
    public class ChequeNumberFormatter
    {
        /// <summary>
        /// The maximum of money value
        /// </summary>
        public const decimal MaxConvertedValue = 2000000000.00M;

        /// <summary>
        /// The count of groups by three digits for the maximum of money value
        /// </summary>
        private const int _maxSegmentsForDollar = 4;

        protected static readonly string[] _firstTwelveNumber = new string[]
        {
            "",
            "one",
            "two",
            "three",
            "four",
            "five",
            "six",
            "seven",
            "eight",
            "nine",
            "ten",
            "eleven",
            "twelve",
            "thirteen",
            "fourteen",
            "fifteen",
            "sixteen",
            "seventeen",
            "eighteen",
            "nineteen",
        };

        protected static readonly string[] _tens = new string[]
        {
            "",
            "",
            "twenty",
            "thirty",
            "forty",
            "fifty",
            "sixty",
            "seventy",
            "eighty",
            "ninety",
        };

        protected static readonly string[] _powers = new string[]
        {
            " billion",
            " million",
            " thousand",
            "",
        };

        /// <summary>
        /// Convert number of money to a string of word (dollars and cents)
        /// </summary>
        /// <param name="value"></param>
        /// <exception cref="ArgumentOutOfRangeException">value less then zero or larger then max</exception>
        /// <returns>a string of word</returns>
        public string GetFormatedString(decimal value)
        {
            if (value < 0 || value > MaxConvertedValue)
            {
                throw new ArgumentOutOfRangeException(nameof(value));
            }

            Debug.Assert(value >= 0 && value <= MaxConvertedValue);

            var sb = new StringBuilder();
            var dollarsHavePlularForm = value < 1 || value >= 2;
            FillGroups(value, out int[] dollarsSegments, out int cents);

            if (value < 10 && dollarsSegments[_maxSegmentsForDollar - 1] == 0)
            {
                sb.Append("zero");
            }
            else
            {
                WriteDollarsSum(dollarsSegments, sb);
            }

            sb.Append(dollarsHavePlularForm ? " dollars" : " dollar");

            if (cents != 0)
            {
                sb
                    .Append(" and ")
                    .Append(TwoDigitsToString(cents));

                sb.Append(cents != 1 ? " cents" : " cent");
            }

            return sb.ToString();
        }

        /// <summary>
        /// Write all groups to buffer
        /// </summary>
        private static void WriteDollarsSum(int[] dollarsSegments, StringBuilder sb)
        {
            for (var index = 0; index < dollarsSegments.Length; index++)
            {
                var segmentValue = dollarsSegments[index];

                if (segmentValue == 0) { continue; }

                if (sb.Length > 0) { sb.Append(", "); }

                WriteGroupValue(sb, segmentValue);
                sb.Append(_powers[index]);
            }
        }

        protected static void WriteGroupValue(StringBuilder sb, int value)
        {
            if (value >= 100)
            {
                int hungred = value / 100;
                value %= 100;
                sb
                    .Append(_firstTwelveNumber[hungred])
                    .Append(" hundred and ");
            }

            sb.Append(TwoDigitsToString(value));
        }

        protected static string TwoDigitsToString(int number)
        {
            Debug.Assert((uint)number < 100);
            if (number < 20) { return _firstTwelveNumber[number]; }

            var firstDigit = number / 10;
            var secondDigit = number % 10;

            return _tens[firstDigit] + (secondDigit == 0 ? "" : (" " + _firstTwelveNumber[secondDigit]));
        }

        protected static string ExtractHundred(int number)
        {
            Debug.Assert(number >= 100 && number < 1000);
            return _firstTwelveNumber[number / 100];
        }

        protected static void FillGroups(decimal value, out int[] dollarGroups, out int centGroup)
        {
            centGroup = (int)Math.Round((value - Math.Floor(value)) * 100);
            dollarGroups = new int[_maxSegmentsForDollar];

            var dollars = Math.Floor(value);
            var index = _maxSegmentsForDollar;

            while (dollars >= 1000)
            {
                dollarGroups[--index] = (int)(dollars % 1000);
                dollars /= 1000;
            }

            dollarGroups[--index] = (int)dollars;
        }
    }
}
