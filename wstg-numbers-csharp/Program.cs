﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace wstg_numbers_csharp
{
    class Program
    {
        static readonly Regex checker = new Regex(@"$\d{0,10}(?:[.,]\d\d?)?");
        static int Main(string[] args)
        {
            Console.Write("Input amount of money:");
            var text = Console.ReadLine();

            try
            {
                if (!checker.IsMatch(text))
                {
                    throw new ArgumentException("Invalid input");
                }

                text = text.Replace(",", ".");
                var value = Decimal.Parse(text, CultureInfo.InvariantCulture);
                var result = new ChequeNumberFormatter().GetFormatedString(value);

                Console.WriteLine(result);
                return 0;
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(e.Message);

                return 1;
            }
        }
    }
}
